/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief PHG Core piping implementation for Cordova on Android.
 *
 * This is the PHG Core piping implemetation for Android, connecting
 * the native reflector to the JVM and JS bridges in a Cordova-based
 * environment.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute, &copy; 2019
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

package dk.s4.phg.cordova_adaptor;

import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.slave.BridgeContext;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.util.Log;



/**
 * @brief PHG Core piping.
 *
 * The BaseplatePiping connects the PHG Core bridge implemetations for
 * Java and JavaScript to the native PHG Core reflector
 * implementation.
 */
public class BaseplatePiping extends CordovaPlugin {
    public static final String TAG = "BaseplatePiping";
    private static final String MODULES_PATH = "modulelist_jvm.txt";

    // Start the native part of the piping (BaseplatePiping.cpp)
    static {
        System.loadLibrary("baseplate-piping");
    }


    // State of the native baseplate
    private enum NativeState {
        NOT_STARTED, FAILED, RUNNING, STOPPED
    }
    private String nativeFailedMessage;
    private NativeState nativeState = NativeState.NOT_STARTED;


    // The JVM Context
    private BridgeContext jvmContext;


    /**
     * Constructor.
     */
    public BaseplatePiping() {
        Log.d(TAG,"*** Constructed; " + Thread.currentThread().getName());
        
        // Initialize native master baseplate with two slaves
        // attached
        try {
            nativeInit(2);
            nativeState = NativeState.RUNNING;
        } catch (RuntimeException e) {
            nativeFailedMessage = e.getMessage();
            nativeState = NativeState.FAILED;
            return; // Do not proceed further
        }
        
        // Initialize JVM baseplate

        jvmContext = new BridgeContext(1) {
                @Override
                protected void bridge2reflector(byte[] data) {
Log.d(TAG,"******* toBridge; len=" + data.length + " " + java.util.Arrays.toString(data));
                    
                    BaseplatePiping.this.toNative(1, data);               
                }
                @Override
                protected synchronized void corePanic(String moduleTag,
                                                      Exception cause,
                                                      boolean fatal) {
                    // Log using screaming colours.
                    if (fatal) {
                        Log.wtf(moduleTag,
                                "\033[1;91mPHG Core Panic, FATAL\033[0m",
                                cause);
                    } else {
                        Log.e(moduleTag,
                              "\033[1;31mPHG Core Panic, ERROR\033[0m",
                              cause);
                    }
                }
            };
        
    }


    // pluginInitialize and onDestroy marks the beginning and end of the
    // application. pluginInitialize also implies onStart and onResume. When
    // the application is closed, onPause and onStop are explicitly called
    // prior to onDestroy.

    // Threading note: The constructor, pluginInitialize() and execute() are
    // executed on the Cordova "WebCore" thread. All the "onX" event handlers
    // are executed on the main (UI) thread.
    
    
    @Override
    public void pluginInitialize() {
        Log.d(TAG,"*** pluginInitialize; " + Thread.currentThread().getName());
    }

    /**
     * The final call you receive before your activity is destroyed.
     */
    @Override
    public void onDestroy() {
        Log.d(TAG,"*** onDestroy; " + Thread.currentThread().getName());
        nativeStop(true,true);
        Log.d(TAG,"*** onDestroy DONE; " + Thread.currentThread().getName());
    }

    
    // Resume and Pause are probably not relevant for us. They are
    // used to indicate is another window (or popup) takes focus when
    // our window is still visible behind it.    

    
    /**
     * Called when the system is about to start resuming a previous
     * activity.
     *
     * @param multitasking Flag indicating if multitasking is turned
     *                     on for app
     */
    @Override
    public void onPause(boolean multitasking) {
        Log.d(TAG,"*** onPause; " + Thread.currentThread().getName());
    }

    /**
     * Called when the activity will start interacting with the user.
     *
     * @param multitasking Flag indicating if multitasking is turned
     *                     on for app
     */
    @Override
    public void onResume(boolean multitasking) {
        Log.d(TAG,"*** onResume; " + Thread.currentThread().getName());
    }


    // onStart and onStop are used to mark when the application window
    // (and therefore the process) is hidden (stopped) and becomes
    // visible (and started).
    
    /**
     * Called when the activity is becoming visible to the user.
     */
    @Override
    public void onStart() {
        Log.d(TAG,"*** onStart; " + Thread.currentThread().getName());
    }

    /**
     * Called when the activity is no longer visible to the user.
     */
    @Override
    public void onStop() {
        Log.d(TAG,"*** onStop; " + Thread.currentThread().getName());
    }


    // Should we be able to handle this situation?
    // Will we require that the webapp must be a Single-Page App?
    // TODO: Talk to Simon/Thomas.

    
    /**
     * Called when the WebView does a top-level navigation or
     * refreshes.
     *
     * Plugins should stop any long-running processes and clean up
     * internal state.
     */
    @Override
    public void onReset() {
        Log.d(TAG,"*** onReset; " + Thread.currentThread().getName());
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              Arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into
     *                          JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    @Override
    public boolean execute(String action, CordovaArgs args,
                           final CallbackContext callbackContext)
                                               throws JSONException {
        if ("pipeInit".equals(action)) {
            Log.d(TAG,"*** pipeInit; " + Thread.currentThread().getName());

            // Check if the baseplate startup was succesful
            if (nativeState != NativeState.RUNNING) {
                callbackContext.error("The native baseplate failed to start: "
                                      + nativeFailedMessage);
                return true;
            }
            
            // Save the pipe connecting Java to JS
            jsPipe = callbackContext;

            // Pipe opened control message
            sendToJS(true, new byte[] {0});

        } else if ("start".equals(action)) {

            // The number of JS modules is provided as an argument

            int moduleCount = args.getInt(0);
            
            
            // Load list of JVM modules
            
            String[] jvmModuleClassNames;
            try {
                jvmModuleClassNames = linesInFile(MODULES_PATH);
            } catch (Exception e) {
                callbackContext.error("Error loading list of JVM modules: "
                                      + e.getMessage());
                return true;
            }
            
            // Module count keeps count of the total number of modules
            // loaded on the entire PHG core system
            moduleCount += jvmModuleClassNames.length;

            
            // Start the native baseplate

            // FIXME: Starting the native baseplate SHOULD actually
            // happen AFTER instantiating the modules. However,
            // because the startup logic in ModuleBase is not finished
            // yet, the subscribe messages are lost and we have to
            // start the native baseplate first. See the FIXME on
            // subscribe/unsubscribe in the ModuleBase.java file.
            Log.d(TAG,"*** STARTING... ");
            started = callbackContext;
            nativeStart(moduleCount);
            
            // Instantiate and start all JVM modules
            // (each module will start a fresh thread on construction)
            
            Log.d(TAG,"*** STARTING "+ moduleCount + " MODULE(S)");
            for (String moduleName : jvmModuleClassNames) {
                Log.d(TAG,"*** STARTING MODULE: " + moduleName);
                try {
                    Class.forName(moduleName)
                        .getConstructor(Context.class, Activity.class)
                        .newInstance(jvmContext, cordova.getActivity());
                } catch (Exception e) {
                    Log.wtf("Failed to load module: " + moduleName, e);
                    callbackContext.error("Error starting module " +
                                          moduleName + ": " +
                                          e.getMessage());
                    return true;
                }
            }

            // FIXME: Temporary hack - We set a 500 ms timer to signal the completion of
            // module initialization. This must be fixed when the baseplate is fully implemented

            (new java.util.Timer()).schedule(new java.util.TimerTask() {
                    @Override
                    public void run() {
                        callbackContext.success();
                    }
                }, 500);
            
        } else if ("pipeDataIn".equals(action)) {
            byte[] msgbuf = args.getArrayBuffer(0);
            toNative(0, msgbuf);   
        } else {
            return false;
        }
        return true;
    }

    private static String[] linesInFile(String modulesFile) throws IOException {
        return linesInStream(getClassLoader().getResourceAsStream(modulesFile));
    }

    private static String[] linesInStream(InputStream stream) throws IOException {
        List<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines.toArray(new String[] {});
    }

    private static ClassLoader getClassLoader() {
        return BaseplatePiping.class.getClassLoader();
    }


    // Native threading guidance:
    //
    // nativeInit() and nativeStart() shall be called in sequence on
    // the same thread at most once each. Attempting to init or start
    // a stopped system is a no-op.
    //
    // nativeStop() can be called at any time on any thread. It will
    // stop the PHG native core if it is running (or prevent it from
    // ever starting). The stop command may be forced (cannot be
    // cancelled). If it is forced, and wait is also set to true, the
    // method will block until the system has properly shut down. The
    // wait argument will be ignored if forced is false - we do not
    // want to allow waiting for a shutdown that might never happen.
    //
    // toNative will be ignored until nativeStart() returns or after
    // the system has been terminated. For each slaveId it must always
    // be called on the same thread (hence, different threads are
    // allowed for different slaveIds)
    //
    // nativeStartDone() and nativeStopDone() are called from other
    // native threads.
    //
    // fromNative() is called from individual slave threads - one per
    // slaveId. So if anything is shared among slaves, this will need
    // to be implemented in a tread-safe manner.
    
    private native void nativeInit(int slaveCount);

    private native void nativeStart(int moduleCount);

    private native void nativeStop(boolean forced, boolean wait);

    private native void toNative(int slaveId, byte[] buffer);

    private void nativeStartDone() {
        // Signal to JS that the startup has completed
        if (started != null) started.success();
        started = null;
    }

    private void nativeStopDone() {
        // Currently unused...
        Log.i(TAG, "*********** Stop Done signalled");
    }
    
    private void fromNative(int slaveId, byte[] buffer) {
        Log.d(TAG,"fromNative " + slaveId + ". Called from: " +
              Thread.currentThread().getName());
        if (slaveId == 0) { // JS baseplate
Log.d(TAG,"****** toJS; len=" + buffer.length + " " + java.util.Arrays.toString(buffer));
            sendToJS(false, buffer);
        } else if (slaveId == 1) { // JVM baseplate
Log.d(TAG,"****** toJVM; len=" + buffer.length + " " + java.util.Arrays.toString(buffer));
            jvmContext.reflector2bridge(buffer);
        } else {
            Log.e(TAG,"Assertion error: Illegal slaveId");
        }
    }


   
    ////////////////
    // FIXME: Threads and possible race conditions. Where do we stand?
    // https://cordova.apache.org/docs/en/latest/guide/platforms/android/plugin.html#threading
    ///////////////


    //////
    // FIXME: How will we ever be asked to close the pipes??
    //        We need also to delete the C++ baseplate at some point!
    //////
    
    
    // The pipe used for data from Java to JS
    private CallbackContext jsPipe = null;
    
    // Callback to signal "startup complete" to JS
    private CallbackContext started = null;


    private void sendToJS(boolean control, byte[] msgbuf) {
        if (jsPipe == null) return;
        
        LinkedList<PluginResult> pr = new LinkedList<PluginResult>();
        // Control or data message...
        pr.add(new PluginResult(PluginResult.Status.OK, control));
        // ...and the payload 
        pr.add(new PluginResult(PluginResult.Status.OK, msgbuf));
        PluginResult res = new PluginResult(PluginResult.Status.OK, pr);
        // Closing the pipe?
        if (!control || msgbuf[0] == 0) res.setKeepCallback(true);
        jsPipe.sendPluginResult(res);
    }
}
