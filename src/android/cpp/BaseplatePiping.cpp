/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief PHG Core piping implementation for Cordova on Android.
 *
 * This is the PHG Core piping implemetation for Android, connecting
 * the native reflector to the JVM and JS bridges in a Cordova-based
 * environment.
 *
 * It is the responsibility of this BaseplatePiping object to start,
 * mangage, and stop all the PHG Core native modules provided by the
 * `modulelist_native.h` file which must be available in the include
 * path.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute, &copy; 2019
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */


#include "MasterModule.hpp"
#include "PipingModule.hpp"
#include "Reflector.hpp"

#include "moduleheaders_native.h"

#include <jni.h>

#include <condition_variable>
#include <iostream>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <vector>



using s4::BasePlate::ApplicationState;
using s4::BasePlate::Context;
using s4::BasePlate::MasterModule;
using s4::BasePlate::ModuleBase;
using s4::BasePlate::PipingModule;
using s4::BasePlate::Reflector;

using std::condition_variable;
using std::lock_guard;
using std::mutex;
using std::set;
using std::thread;
using std::to_string;
using std::vector;
using std::unique_lock;
using std::unique_ptr;

/* 
 * The only exported elements from this file are the JNI methods used
 * by BaseplatePiping.java found at the bottom of this file.
 */
namespace {

  
  // Forward declaration
  void coreStopWithStateUpdate();
    
  /**
   * The BaseplatePiping object is a singleton object that wraps all the
   * PHG Core native modules and baseplate. 
   */
  class BaseplatePiping {

    /** "Shortcut" to the Java BaseplatePiping.fromNative() method */
    jmethodID fromNative;

    /** The Context object shared among all native modules */
    Context context;

    /** The master baseplate reflector */
    Reflector reflector;

    /**
     * The MasterModule in charge of orchestration/teardown of the
     * entire PHG core system
     */
    MasterModule *masterModule = nullptr;

    /** The native modules attached to the native baseplate */
    vector<unique_ptr<ModuleBase> > modules;

    /**
     * A PipingModule is a "normal" module attached to the master
     * baseplate, acting as a proxy for all modules attached to a
     * slave baseplate. For each slave baseplate, one instance of this
     * class will be created.
     */
    class PM : public PipingModule {
      int id; // The index of this PM object in the list of all PMs
      BaseplatePiping &parent;
    public:
      PM(Context &context, BaseplatePiping &parent, int id)
        : PipingModule(context,"PipingModule[" + to_string(id) + "]"),
          id(id), parent(parent) {}

      
      // Start the PipingModule thread
      void start() {
        PipingModule::start();

        // Clean up by detaching the module's thread from the Java VM
        setFinalCallback([this] () -> void {
            // Detach thread
            this->parent.vm->DetachCurrentThread();
          });
        
      }
      
      // Implementing the virtual reflector-to-bridge pipe
      void toBridge(vector<int8_t> buf) {

        // Get the JNIEnv 
        JNIEnv *env;
        parent.vm->GetEnv((void**)&env, JNI_VERSION_1_2);

        // The first time, we are not attached yet
        if (!env) {
          JavaVMAttachArgs args;
          args.version = JNI_VERSION_1_2;
          args.name = string("PipingModule[" + to_string(id) + "] thread").c_str();
          args.group = NULL;
          if (parent.vm->AttachCurrentThread(&env, &args) != JNI_OK) {
            return;
          }
        }

        // Copy C++ vector buf to Java buffer
        jbyteArray buffer = env->NewByteArray(buf.size());
        env->SetByteArrayRegion(buffer,0,buf.size(),buf.data());

        // Call the Java BaseplatePiping.fromNative() method
        env->CallVoidMethod(parent.pipe, parent.fromNative, id, buffer);

        // Release the Java buffer copy
        env->DeleteLocalRef(buffer);
      }
    
    };

    /** The PipingModules for all the slave baseplates */
    vector<unique_ptr<PM> > pipingModules;


  public:
    
    /** The Java VM */
    JavaVM* vm;

    /** 
     * The corresponding Java BaseplatePiping object (which must also
     * be singleton)
     */
    jobject pipe;
    

    BaseplatePiping(JNIEnv* env, jobject pipe, int slaveCount)
      : fromNative(env->GetMethodID(env->GetObjectClass(pipe),
                                    "fromNative", "(I[B)V")),
        // Create the Context
        context("inproc://module2reflector",
                "inproc://reflector2module","Master"),
        // ... and Reflector
        reflector(&context),
        // We keep a reference to the Java BaseplatePiping object
        // around
        pipe(env->NewGlobalRef(pipe)) {

      // Create one PipingModule per slave
      for (int i=0; i<slaveCount; i++) {
        pipingModules.emplace_back(new PM(context,*this,i));
      }

      // And last but not least, store the Java VM handle
      env->GetJavaVM(&vm);
    }

    /**
     * The destructor does NOT release the global pipe reference.
     * This must be done after the BaseplatePiping object has been
     * deleted
     */
    ~BaseplatePiping() {
      
      // Wait for modules to finish and then clean up
      modules.clear();
      
      // And the same goes for the PipingModules...
      pipingModules.clear();
      
      // Wait for the MasterModule and clean it up as well
      if (masterModule) {
        delete masterModule;
        masterModule = nullptr;
      }
    }

    /**
     * Implementing the bridge-to-reflector pipe
     */
    void toNative(JNIEnv* env, jint slaveId, jbyteArray buffer) {
      // Copy Java buffer to C++ vector buf
      jint arraylen = env->GetArrayLength(buffer);
      int8_t *array = env->GetByteArrayElements(buffer, NULL); 
      vector<int8_t> buf(array, array + arraylen);
      env->ReleaseByteArrayElements(buffer,array,JNI_ABORT);

      // Submit buffer from bridge #slaveId to the native baseplate
      // through its dedicated PipingModule
      pipingModules[slaveId]->fromBridge(buf);
    }

    
    void start(JNIEnv* env, int moduleCount) {

      // Start the PipingModules
      for (auto&& pm : pipingModules) pm->start();
      

      // Load native modules
      //
      // The modules are listed, each as a single line on the form
      //
      //     MODULE(fully::qualified::ModuleClassName)
      //
      // in the `modulelist_native.h`file. 

#define MODULE(M) modules.emplace_back(new M(context));
#include "modulelist_native.h"
 
      // Increase the moduleCount by the number of modules,
      // pipingModules and the MasterModule itself

      moduleCount += modules.size() + pipingModules.size() + 1;

      // Load the MasterModule

      masterModule = new MasterModule(context, moduleCount);

      // Signal startDone to the app when the entire PHG Core is up
      // and running

      masterModule->addStateCallback(ApplicationState::RUNNING,
        [this] () -> void {

          // Attach the MasterModule thread temporarily to the Java VM
          JNIEnv *env;
          JavaVMAttachArgs args;
          args.version = JNI_VERSION_1_2;
          args.name = "nativeStartDone() signal thread";
          args.group = NULL;
          if (vm->AttachCurrentThread(&env, &args) != JNI_OK) {
            return;
          }
          
          // Signal the event to Java
          env->CallVoidMethod(pipe, env->GetMethodID(
                         env->GetObjectClass(pipe),
                         "nativeStartDone", "()V"));
          
          // ... and detach the thread from the VM
          vm->DetachCurrentThread();
      });
      
      // Catch the FINALIZING signal and initiate a Core shutdown

      masterModule->addStateCallback(ApplicationState::FINALIZING,
                                     coreStopWithStateUpdate);
      
      // Start the reflector
      
      reflector.start();
      
    }

    void requestShutdown(bool forced) {
      masterModule->requestShutdown(forced);
    }
    
  };


  
  /**
   * The singleton BaseplatePiping object
   */
  BaseplatePiping *pipeSingleton = nullptr;

  
  /**
   * The overall state of the PHG native core.  
   *
   * FIXME: A graph + explanation of the states and transitions should
   * go here
   */
  enum {
    UNINITIALIZED,
    INITIALIZING,
    INITIALIZED,
    STARTING,
    STARTED,
    STOP_PENDING,
    STOP_INITIATED,
    STOPPING,
    STOPPED
  } state = UNINITIALIZED;
  mutex stateLock; // Protecting multi-threaded access to state
  condition_variable stateCV; // Notified on transitions to STOPPED
  
  /**
   * Spawn a cleanup thread for the PHG core destruction
   */
  void coreStop() {

    // Start a thread to perform the cleanup
    thread([]() -> void {
        // Test and update the state machine
        {
          lock_guard<mutex> lock(stateLock);
          if (state != STOP_INITIATED) {
            // Shouldn't happen
            return;
          }
          state = STOPPING;
          // Release lock
        }
        if (pipeSingleton) {
          JavaVM *vm = pipeSingleton->vm;
          jobject pipe = pipeSingleton->pipe;

          // Destroy the BaseplatePiping object. The destructor will
          // block for the termination of the entire PHG core and the
          // cleanup of all native core parts.
          delete pipeSingleton;
          pipeSingleton = nullptr;

          
          // Get the JNIEnv 
          JNIEnv *env;
          JavaVMAttachArgs args;
          args.version = JNI_VERSION_1_2;
          args.name = "PHG core destructor thread";
          args.group = NULL;
          if (vm->AttachCurrentThread(&env, &args) != JNI_OK) {
            return;
          }
          
          // Signal the event to Java
          env->CallVoidMethod(pipe, env->GetMethodID(
                         env->GetObjectClass(pipe),
                         "nativeStopDone", "()V"));
          
          // Release the reference to the Java BaseplatePiping object
          env->DeleteGlobalRef(pipe);

          // ... and detach the thread from the VM
          vm->DetachCurrentThread();
        }
        // Update the state machine
        {
          lock_guard<mutex> lock(stateLock);
          state = STOPPED;
          stateCV.notify_all();
          // Release lock
        }
      }).detach();
  }

  /**
   * Attempt to make a STARTED->STOP_INITIATED state transition, and if
   * successful spawn a cleanup thread
   */
  void coreStopWithStateUpdate() {
    // Test and update the state machine
    {
      lock_guard<mutex> lock(stateLock);
      if (state != STARTED) {
        // We're in the wront state, nothing to do here
        return;
      }
      state = STOP_INITIATED;

      // Release lock
    }
    // Spawn a cleanup thread
    coreStop();
  }

  
  /*
   * JNI method definition:
   *
   * private native void nativeInit(int slaveCount);
   */
  
  extern "C" JNIEXPORT void JNICALL
  Java_dk_s4_phg_cordova_1adaptor_BaseplatePiping_nativeInit
  (JNIEnv* env, jobject pipe, jint slaveCount) {

    // Test and update the state machine
    {
      lock_guard<mutex> lock(stateLock);
      // In case a concurrent thread issued a stop, we won't complain
      if (state == STOPPED) return;
      
      if (state != UNINITIALIZED) {
        // Throw a RuntimeException if multiple nativeInit invocations
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
                      "nativeInit() invoked multiple times");
        return;
      }
      if (pipeSingleton) { // Shouldn't happen
        // Throw a RuntimeException if multiple objects co-exist
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
               "Illegal internal state: pipeSingleton already defined");
        return;
      }
      
      state = INITIALIZING;
      // Release lock
    }
   
    
    // Construct a matching C++ BasePlatePiping object and link the two
    pipeSingleton = new BaseplatePiping(env, pipe, slaveCount);

    
    // Test and update the state machine
    {
      lock_guard<mutex> lock(stateLock);
      if (state == INITIALIZING) {
        state = INITIALIZED;
        return;
      } else if (state == STOP_PENDING) {
        state = STOP_INITIATED;
        // Don't return - we continue below
      } else { // Shouldn't happen
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
                      "Illegal internal state");
        return;
      }
      // Release lock
    }
    // Spawn a cleanup thread
    coreStop();
  }

  
  /*
   * JNI method definition:
   *
   * private native void nativeStart(int moduleCount);
   */
  
  extern "C" JNIEXPORT void JNICALL
  Java_dk_s4_phg_cordova_1adaptor_BaseplatePiping_nativeStart
  (JNIEnv* env, jobject pipe, jint moduleCount) {
    
    // Test and update the state machine
    {
      lock_guard<mutex> lock(stateLock);
      switch (state) {
      case INITIALIZED:
        break; // Continue below

      case STOP_INITIATED:
      case STOPPING:
      case STOPPED:
        // This is legal: if nativeStart is called AFTER nativeStop,
        // it must be completely ignored
        return;


      default: // UNINITIALIZED,INITIALIZING,STARTING,STARTED,STOP_PENDING
        // Throw a RuntimeException: nativeStart invoked in a wrong state
        // Either nativeInit() has not yet returned or nativeStart() was
        // invoked multiple times
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
                      "nativeStart() invoked in a wrong state");
        return;
      }
      
      if (!pipeSingleton) { // Shouldn't happen
        // Throw a RuntimeException if no object exist
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
              "Illegal internal state: the BaseplatePiping reference is null");
        return;
      }
      if (!env->IsSameObject(pipe,pipeSingleton->pipe)) {
        // Throw a RuntimeException if not singleton
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
           "This native BaseplatePiping called from different Java objects");
        return;
      }
      
      state = STARTING;
      // Release lock
    }
   
    pipeSingleton->start(env,moduleCount);

        
    // Test and update the state machine
    {
      lock_guard<mutex> lock(stateLock);
      if (state == STARTING) {
        state = STARTED;
      } else if (state == STOP_PENDING) {
        state = STARTED;

        // We are in the STARTED state and must tell the MasterModule to
        // shut down the core system for us - and then await the core
        // shutdown confirmation.
        // Send the message while holding the lock - this is a short,
        // non-blocking task

        pipeSingleton->requestShutdown(true);
        
      } else { // Shouldn't happen
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
                      "Illegal internal state");
      }
      // Release lock
    }
  }

  
  /*
   * JNI method definition:
   *
   * private native boolean nativeStop(boolean forced);
   */
  
  extern "C" JNIEXPORT void JNICALL
  Java_dk_s4_phg_cordova_1adaptor_BaseplatePiping_nativeStop
  (JNIEnv* env, jobject pipe, jboolean forced, jboolean wait) {

    // We do not want to risk waiting for a shutdown that was
    // cancelled.
    if (!forced) wait = false;

    bool initiateStop = false; // Transitioned into the STOP_INITIATED state

    // Test and update the state machine
    {
      lock_guard<mutex> lock(stateLock);
      switch (state) {
      case UNINITIALIZED:
        state = STOPPED;
        // Return immediately as there is nothing to stop - or wait for
        return;

      case INITIALIZING:
      case STARTING:
        state = STOP_PENDING;
        // Don't initiate stop - will be initiated later
        break;
        
      case INITIALIZED:
        state = STOP_INITIATED;
        initiateStop = true;
        break;

      case STARTED:
        // Don't initiate stop - will be initiated later
        break;
        
      default: // STOP_PENDING, STOP_INITIATED, STOPPING, STOPPED
        // Don't initiate stop - already initiated
        break;
      }
      

      if (state == STARTED) {

        if (!pipeSingleton) { // Shouldn't happen
          // Throw a RuntimeException if no object exist
          env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
              "Illegal internal state: the BaseplatePiping reference is null");
          return;
        }
        if (!env->IsSameObject(pipe,pipeSingleton->pipe)) {
          // Throw a RuntimeException if not singleton
          env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
              "This native BaseplatePiping called from different Java objects");
          return;
        }
        
        // We are in the STARTED state and must tell the MasterModule to
        // shut down the core system for us - and then await the core
        // shutdown confirmation.
        // Send the message while holding the lock - this is a short,
        // non-blocking task

        pipeSingleton->requestShutdown(forced);
        return;
        
      }
      
      // Release lock
    }

    if (initiateStop) {
      // Spawn a cleanup thread
      coreStop();
    }

    if (wait) {
      // Wait for the state to transition to STOPPED
      {
        unique_lock<mutex> lock(stateLock);
        while (state != STOPPED) stateCV.wait(lock);
      }
    }
  }
  
  
  /*
   * JNI method definition:
   *
   * private native void toNative(int slaveId, byte[] buffer);
   */
  
  extern "C" JNIEXPORT void JNICALL
  Java_dk_s4_phg_cordova_1adaptor_BaseplatePiping_toNative
  (JNIEnv* env, jobject pipe, jint slaveId, jbyteArray buffer) {
  
    // Test and update the state machine
    {
      lock_guard<mutex> lock(stateLock);
      // In all other states, the message is silently dropped.
      if (state != STARTED) return;
      
      if (!pipeSingleton) { // Shouldn't happen
        // Throw a RuntimeException if no object exist
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
              "Illegal internal state: the BaseplatePiping reference is null");
        return;
      }
      if (!env->IsSameObject(pipe,pipeSingleton->pipe)) {
        // Throw a RuntimeException if not singleton
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"),
           "This native BaseplatePiping called from different Java objects");
        return;
      }

      // Send the message while holding the lock - this is a short,
      // non-blocking task
      pipeSingleton->toNative(env,slaveId,buffer);
      
      // Release lock
    }
  }
}
