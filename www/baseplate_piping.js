/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief JavaScript frontend for the Cordova plugin.
 *
 * This is the JavaScript frontend for connecting the 4S PHG core JS
 * baseplate bridge from a browser context using Cordova as the
 * platform connector to the native baseplate reflector.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute, &copy; 2019
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
var exec = require('cordova/exec');

function baseplateInit(baseplate, callback) {
    console.log('********* BaseplatePiping started');
    var pipeout = undefined;
    var pipein = undefined;

    // Initialize the native subsystem - when complete, it will
    // inform the callback "pipe opened", which will then
    // initialize the JavaScript object
    exec(function (control, msgbuf) {

        // control:   true if msgbuf is a control message, and
        //            false if it is pipe data.
        // msgbuf:    ArrayBuffer containing the payload

        if (control) {
            // The following control messages are currently defined:
            // [0]: Pipe opened
            // [1]: Pipe closed

            var bytearray = new Uint8Array(msgbuf);
            if (bytearray[0] == 0) {
                console.log('********* pipe open');

                pipein = function (buf) {
                    exec(null, null, "BaseplatePiping", "pipeDataIn", [buf]);
                };

                pipeout = baseplate.initialize(function (buf) { pipein && pipein(buf); });

                var moduleCount = 0;

                // (Currently we have none and do not expect to ever have any)
                
                // Report the number of JS modules to the master baseplate
                // and await startup of the entire PHG Core subsystem

                exec(function () {
                    var msg = "Baseplate succesfully initialized"
                    console.log(msg)
                    callback.success(msg)
                }, function (e) {
                    var msg = "[ERROR] Error initializing 4S PHG Core Cordova plugin: " + e
                    console.log(msg);
                    callback.error(msg)
                }, "BaseplatePiping", "start", [moduleCount]);
            } else if (bytearray[0] == 1) {
                console.log('********* pipe closed');

                // The pipe is broken
                pipeout = undefined;
                pipein = undefined;
            }
        } else {
            pipeout && pipeout(msgbuf);
        }
    }, function (e) {
        var msg = "[ERROR] Error initializing 4S PHG Core Cordova plugin: " + e
        console.log(msg);
        callback.error(msg)
    }, "BaseplatePiping", "pipeInit", []);
}

// Create and export the Baseplate object
module.exports = baseplateInit
