Cordova adaptor for 4S PHGs
===========================
The cordova adaptor is a special project because it ties the three 
baseplates (js/java/native) together and allows starting them up
within the cordova framework(https://cordova.apache.org).

Function
--------
This section details what the cordova adaptor does.

### Piping
If an app initiates listening for a certain type of messages, for
example blodsugar measurements, that "subscribe" message is
sent to the javascript baseplate because the app is written in
javascript.
Then that "subscribe" needs to be passed on the other baseplates,
that is the java baseplate and the native baseplate. Where a
module will pick up the "subscribe".

Piping is the code that sends messages between the baseplates
that is written in different languages.

The cordova-adaptor builds that piping.

### Starting baseplates and modules
Besides piping the cordova adaptor also handles starting the
basplates and all the modules that has been installed.

When the javascript app is ready the cordova adaptor can be told
to start the basplates, the modules and setup the piping.

The list of installed modules is determined by scanning 
through for module_name.txt files that contain module names.

module_name.txt files are created and copied by cordova-wrapper
projects, for example the cdvw-bluetooth(bitbucket.org/4s/cdvw-bluetooth)
and cdvw-monica(bitbucket.org/4s/cdvw-monica-ctg)

### js-module
This project exposes a javascript module via cordova, that module
is called to let the cordova-adaptor do its initialization described
in the previous.

Dependencies
------------
The jvm-baseplate is pulled in via java-dependencies.gradle and
the native-baseplate via conan.
The javascript baseplate is pulled in by the app.

Using
-----
In order for the app to start the baseplates and modules, it needs
to use the js-module of this project to start initialization.

The javascript baseplate is required as a parameter to initialization.

An example of doing that in an ionic app in the app.components.ts
could be to import baseplate:
```
import * as wrapper from 'phg-js-baseplate';
```
As part of app initialization do:
```
  initializeApp() {
    this.platform.ready().then(() => {
      this.initializeBaseplate();
    }
  }

 initializeBaseplate() {
     let piping = cordova.require('dk.s4.phg.cordova-adaptor.BaseplatePiping');
     piping(wrapper, this.baseplateCallback);
 }
```
Where the baseplateCallback is an object with error(error: string) and 
success(meassage: string) callback methods.

Documentation
-------------
An general introduction to the the 4S PHG module collection is
available at the [4S website](http://4s-online.dk/PHG/core/). This
would be the place to begin for newcomers.

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.

License
-------
The 4SDC library is licensed under the [Apache 2.0
license](http://www.apache.org/licenses/LICENSE-2.0).